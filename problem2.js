/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const fs = require("fs");
const { type } = require("os");
const path = require("path");
const { resolve } = require("path");
const { reject } = require("promise");
const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';



read();
async function read() {

    // appends data to given file
    function appendToFile(dataToAppend, fileName) {
        const appendToFilePromise = new Promise((resolve, reject) => {
            fs.appendFile(fileName, `\n${dataToAppend}`, (err) => {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                else {
                    resolve();
                }
            })
        })
        return appendToFilePromise;
    }

    // create a new file with given path
    function createFile(fileName) {
        const createFilePromise = new Promise((resolve, reject) => {

            {
                fs.writeFile(fileName, "", (err) => {
                    if (err) {
                        console.log(err);
                        reject(err)
                    }
                    else {
                        console.log(fileName + " created.");
                        resolve();
                    }
                })
            }
        })
        return createFilePromise;
    }

    // read file with given name
    function readFile(fileName) {
        const readFilePromise = new Promise((resolve, reject) => {
            fs.readFile(fileName, 'utf8', (err, data) => {
                if (err) {
                    reject(err);
                    throw err;
                }
                else {
                    console.log("read file" + fileName);
                    resolve(data);
                }
            })
        })
        return readFilePromise;
    }

    // converts data to upper case
    function toUpperCase(content) {
        const toUpperCasePromise = new Promise((resolve, reject) => {
            if (typeof (content) !== 'string') {
                reject("not a string");
            } else {
                resolve(content.toUpperCase());
            }
        })
        return toUpperCasePromise;
    }

    // write data in alredy existing file
    function writeToFile(content, fileName) {
        const writeToFilePromise = new Promise((resolve, reject) => {
            fs.writeFile(fileName, content, (err) => {
                if (err) {
                    reject(err);
                    throw err;
                }
                else {
                    console.log("written data to " + fileName);
                    resolve();
                }
            })
        })
        return writeToFilePromise;
    }

    // function to convert data to uppercase and write in a file
    async function writeUpperCase(content, upperCaseFile) {

        try {
            const upperCaseData = await toUpperCase(content);
            await writeToFile(upperCaseData, upperCaseFile);
            resolve();
        } catch (error) {
            console.log(error);
            reject(error);
        }
    }

    // create new directory with given path
    function createdirectory(dirName) {
        if (fs.existsSync(dirName) == 0) {
            fs.mkdir(dirName, (err) => {
                if (err) {
                    reject(err);
                    throw err;
                }
                else {
                    resolve();
                    console.log("created " + dirName);
                }
            })
        }
    }

    // generate random txt file names
    function generateRandomFileName(dirPath) {
        let randomString = '';
        const charactersLength = 5;

        for (let i = 0; i < charactersLength; i++) {
            randomString += characters.charAt(Math.floor(Math.random() * characters.length));
        }

        return (dirPath + "/" + randomString + ".txt");
    }


    // sorts given string data according to each word
    function sortString(data) {
        return data.split(" ").sort().join(" ");
    }

    // remove file with given path
    function removeFile(fileName) {
        const currentremovePromise = new Promise((resolve, reject) => {
            fs.unlink(`${fileName}`, (err) => {
                if (err) {
                    if (err.code == "ENOENT") {
                        // console.log("already deleted");
                        resolve();
                    }
                    else {
                        reject(err);
                        throw err;
                    }

                }
                else {
                    console.log("deleted " + fileName);
                    resolve();
                }
            })
        })
        return currentremovePromise;
    }

    // delete files mentioned in a given container file
    async function deleteFiles(containerFile) {
        try {
            const data = await readFile(containerFile);
            const fileNamesArray = data.split("\n");
            for (const files in fileNamesArray) {
                removeFile(fileNamesArray[files]);
            }
            console.log("deleted all files")
            resolve();
        } catch (error) {
            console.log(error);
            reject(error);
        }


    }

    // from array of strings create new files with random names with each string in array
    async function writeIntoNewFiles(sentences, dirPath, nameLogFile) {

        try {
            await createdirectory(dirPath);
            let i = 0;
            while (i < sentences.length) {
                const data = sentences[i];
                const currentName = generateRandomFileName(dirPath);
                if (fs.existsSync(currentName) == 0) {
                    await writeToFile(data, currentName);
                    await appendToFile(currentName, nameLogFile);
                    i++;
                }
            }
            console.log("sentences writen in random files and filenames appended in" + nameLogFile)
            resolve();
        } catch (error) {
            console.log(error);
            reject(error);
        }
    }

    // main logic
    try {
        const lipsumFile = "./lipsum.txt"
        const upperCaseFile = "./upperCase.txt";
        const logFile = "./filenames.txt";
        const newFileName1 = "./newFile.txt";
        const newFileName2 = "./newFile2.txt";
        const sentencesDirectory = "./sentenceFiles";
        const sortedFile = "./sorted.txt";

        // 1. Read the given file (lipsum.txt)

        const fileData = await readFile(lipsumFile);


        // 2. Convert the contents of the file lipsum.txt to uppercase & write the uppercase content to a new file called uppercase.txt. Store the name of the new file in filenames.txt

        // create (filenames.txt)
        await createFile(logFile);

        // write (lipsum.txt) data in uppercase to (upperCase.txt)
        await writeUpperCase(fileData, upperCaseFile);

        // append (uppercase.txt) to (filenames.txt)
        await appendToFile(upperCaseFile, logFile);


        // 3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write each sentence into separate new files. Store the name of the new files in filenames.txt 

        // Read (newFile.txt) 
        const newFileContent = await readFile(newFileName1);

        // convert data of (newFile.txt) to lowercase
        // seprate into different sentences
        // storing in (sentenceArray)
        const sentenceArray = newFileContent.toLowerCase().split(".");

        // write each sentence in new files in (sentencesFiles directory)
        // and log new file names in (fileNames.txt)
        await writeIntoNewFiles(sentenceArray, sentencesDirectory, logFile);



        // 4. Read the new files, sort the content, write it out to a new file, called sorted.txt. Store the name of the new file in filenames.txt

        // read data from (newFile2.txt)
        const fileData2 = await readFile(newFileName2);

        // create file (sorted.txt)
        await createFile(sortedFile);
        // sort (newFile2.txt) data
        sortedData = sortString(fileData2);
        // write sorted data to (sorted.txt)
        await writeToFile(sortedData, sortedFile);
        // append (sorted.txt) to (filenames.txt)
        await appendToFile(sortedFile, logFile);


        // 5. Read the contents of (filenames.txt) and delete all the new files that are mentioned in that list concurrently.

        // delete all files mentioned in (filenames.txt)
        await deleteFiles(logFile);

    } catch (error) {
        console.log(error);
        return;
    }
}

module.exports = read;