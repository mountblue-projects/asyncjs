/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const fs = require("fs");
const promise = require("promise");
const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
const dirName = "randomJSONFiles";
const relativePath = "./randomJSONFiles/";
const numberOfFiles = 3;

function generateRandomFileName() {
    let randomString = '';
    const charactersLength = 5;

    for (let i = 0; i < charactersLength; i++) {
        randomString += characters.charAt(Math.floor(Math.random() * characters.length));
    }

    return randomString;
}

// Part 1: Implement the above using callbacks and the fs module's asynchronous functions. Don't use promises or async / await

// generating random strings for file names

// function to createDirectory  and implement other callbacks
function createDirectory(dirName, relativePath, numberOfFiles, callback, deleteFilesFunction) {
    try {
        fs.mkdir(dirName, (err) => {
            if (err) {
                if (err.code === 'EEXIST') {
                    console.log("directory already exists");
                    callback(relativePath, numberOfFiles, deleteFilesFunction);
                }
                else {
                    console.log("err in callback of createDirectory");
                    throw err;
                }
            }
            else {
                console.log("created directory");
                callback(relativePath, numberOfFiles, deleteFilesFunction);
            }
        });
    } catch (error) {
        console.log("error err in createDirectory");
    }
}

// to create random JSON files
function createRandomFiles(relativePath, numberOfFiles, deleteFilesFunction) {
    try {
        let i = 0;
        while (i < numberOfFiles) {
            let fileName = `${relativePath}${generateRandomFileName()}.json`;
            fs.writeFile(fileName, JSON.stringify(Math.random), (err) => {
                if (err) {
                    console.log(err);
                    throw err;
                }
                else {
                    console.log("created " + fileName);
                    deleteFilesFunction(fileName);
                }
            })
            i++;
        }
    } catch (error) {
        console.log("error in createRandomFiles");
    }
}

// to delete created files
function deleteFiles(fileName) {
    try {
        fs.unlink(fileName, (err) => {
            if (err) {
                console.error("Error deleting " + fileName + ":" + err);
            } else {
                console.log("Deleted " + fileName);
            }
        });
    } catch (err) {
        console.log("error in deleting " + fileName);
        throw err;
    }

}
function part1() {
    createDirectory(dirName, relativePath, numberOfFiles, createRandomFiles, deleteFiles);
}
// part1();



// Part 2: Implement the above using callbacks and promises. Don't use async / await.

function createDirectoryPart2(dirName) {
    const createDirPromise = new promise((resolve, reject) => {
        fs.mkdir(dirName, (err) => {
            if (err) {
                if (err.code === 'EEXIST') {
                    console.log("directory exists");
                    resolve();
                } else {
                    reject(err);
                }
            }
            else {
                console.log("created directory");
                resolve();
            }
        })
    });
    return createDirPromise;
}



function createFilesPart2(fileName) {
    const createFilePromise = new promise((resolve, reject) => {
        fs.writeFile(fileName, JSON.stringify(Math.random()), (err) => {
            if (err) {
                // if (err.code === 'EEXIST') {

                // }
                console.log("error in creating file");
                reject(err);
            }
            else {
                console.log("created file " + fileName);
                resolve();
            }
        })
    })
    return createFilePromise;
}

function deleteFilesPart2(fileName) {
    const currentremovePromise = new promise((resolve, reject) => {
        fs.unlink(`${fileName}`, (err) => {
            if (err) {
                reject(err);
                throw err;
            }
            else {
                console.log("deleted " + fileName);
                resolve();
            }
        })
    })
    return currentremovePromise;
}

function createAndDeleteFiles(relativePath, numberOfFiles, generateRandomFileName) {
    let promiseArray = [];
    let i = 0;
    while (i < numberOfFiles) {
        let currentFileName = `${relativePath}/${generateRandomFileName()}.json`;
        let currentPromise = new Promise((resolve, reject) => {

            return createFilesPart2(currentFileName)
                .then(() => {
                    resolve(deleteFilesPart2(currentFileName))
                })
                .catch((err) => {
                    reject(err)
                })
        });
        promiseArray.push(currentPromise);
        i++;
    }
    return promise.all(promiseArray);
}

function part2() {
    createDirectoryPart2(dirName)
        .then((response) => { return createAndDeleteFiles(dirName, numberOfFiles, generateRandomFileName) })
        .catch((err) => { console.log(err) });
}

// part2();





// Part 3: Implement the above using async / await

// part3();

async function part3() {
    const dirName = "randomJSONFiles";
    // const relativePath = "./randomJSONFiles/";
    const numberOfFiles = 3;
    function createDirectoryUsingAsync(dirName) {
        const createDirPromise = new promise((resolve, reject) => {
            fs.mkdir(dirName, (err) => {
                if (err) {
                    if (err.code === 'EEXIST') {
                        console.log("directory exists");
                        resolve();
                    } else {
                        reject(err);
                    }
                }
                else {
                    console.log("created directory");
                    resolve();
                }
            })
        });
        return createDirPromise;
    }



    function createFilesUsingAsync(fileName) {
        const createFilePromise = new promise((resolve, reject) => {
            fs.writeFile(fileName, JSON.stringify(Math.random()), (err) => {
                if (err) {
                    // if (err.code === 'EEXIST') {

                    // }
                    console.log("error in creating file");
                    reject(err);
                }
                else {
                    console.log("created file " + fileName);
                    resolve();
                }
            })
        })
        return createFilePromise;
    }

    function deleteFilesUsingAsync(fileName) {
        const currentremovePromise = new promise((resolve, reject) => {
            fs.unlink(`${fileName}`, (err) => {
                if (err) {
                    reject(err);
                    throw err;
                }
                else {
                    console.log("deleted " + fileName);
                    resolve();
                }
            })
        })
        return currentremovePromise;
    }

    function createAndDeleteFilesInAsync(relativePath, numberOfFiles, generateRandomFileName) {
        let promiseArray = [];
        let i = 0;
        while (i < numberOfFiles) {
            let currentFileName = `${relativePath}/${generateRandomFileName()}.json`;
            let currentPromise = new Promise((resolve, reject) => {

                return createFilesUsingAsync(currentFileName)
                    .then(() => {
                        resolve(deleteFilesUsingAsync(currentFileName))
                    })
                    .catch((err) => {
                        reject(err)
                    })
            });
            promiseArray.push(currentPromise);
            i++;
        }
        return promise.all(promiseArray);
    }




    try {
        const createDirectoryPart3 = await createDirectoryUsingAsync(dirName);
        console.log("started with creation")
        const createFilesPart3 = await createAndDeleteFilesInAsync(dirName, numberOfFiles, generateRandomFileName);

    } catch (error) {
        console.log((error));
    }
}


module.exports = { part1, part2, part3 };